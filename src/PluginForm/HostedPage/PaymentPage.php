<?php

namespace Drupal\commerce_paytabs\PluginForm\HostedPage;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_paytabs\PaytabsApi;
use Drupal\commerce_paytabs\PaytabsEnum;
use Drupal\commerce_paytabs\PaytabsRequestHolder;
use Drupal\commerce_paytabs\PaytabsSDK;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Paytabs hosted payment page.
 */
class PaymentPage extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The paytabs SDK.
   *
   * @var \Drupal\commerce_paytabs\PaytabsSDK
   */
  protected $paytabsSdk;

  /**
   * Constructs a new HyperpayForm object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list.
   * @param Drupal\Core\Logger\LoggerChannelInterface $logger_channel_factory
   *   The logger.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(LanguageManagerInterface $language_manager, CountryRepositoryInterface $country_repository, ModuleExtensionList $module_extension_list = NULL, LoggerChannelFactoryInterface $logger_channel_factory, MessengerInterface $messenger) {
    $this->paytabs_sdk = new PaytabsSDK();
    $this->languageManager = $language_manager;
    $this->countryRepository = $country_repository;
    $this->moduleExtensionList = $module_extension_list;
    $this->logger = $logger_channel_factory->get('commerce_paytabs');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('address.country_repository'),
      $container->get('extension.list.module'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $order = $payment->getOrder();
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $config = $payment_gateway_plugin->getConfiguration();
    $payment_amount = $payment->getAmount();
    $customer = $order->getCustomer();

    $paytabs_core = new PaytabsRequestHolder();
    $paytabs_api = PaytabsApi::getInstance($config['region'], $config['profile_id'], $config['server_key']);

    $transaction_title = $this->t('Order Number: @order_number', ['@order_number' => $order->id()]);

    $billing_profile = $order->getBillingProfile();
    $billing_phone = $billing_profile->hasField($config['telephone_field']) ? $billing_profile->get($config['telephone_field'])->value : 00000000000;
    $billing_info = $billing_profile->address->first();
    $billing_country_code = $billing_info->getCountryCode();
    $billing_country = $this->countryRepository->get($billing_country_code)->getThreeLetterCode();
    $billing_full_name = $billing_info->getGivenName() . ' ' . $billing_info->getFamilyName();

    $platform_version = $this->moduleExtensionList->getExtensionInfo('commerce')['version'] ?? 0;
    $plugin_version = $this->moduleExtensionList->getExtensionInfo('commerce_paytabs')['version'] ?? 0;

    $frammed = $config['iframe'] == 'true' ? TRUE : FALSE;
    $hide_shipping = $config['hide_shipping_address'] == 'true' ? TRUE : FALSE;

    $paytabs_core
      ->set01PaymentCode('all')
      ->set02Transaction($config['pay_page_mode'], 'ecom')
      ->set03Cart($order->id(), $payment_amount->getCurrencyCode(), floatval($payment_amount->getNumber()), $transaction_title)
      ->set04CustomerDetails($billing_full_name, $customer->getEmail(), $billing_phone, $billing_info->getAddressLine1(), $billing_info->getLocality(), $billing_info->getAdministrativeArea() ? str_replace(" Governorate", "", $billing_info->getAdministrativeArea()) : $billing_info->getLocality(), $billing_country, $billing_info->getPostalCode() ? $billing_info->getPostalCode() : '00000', $order->getIpAddress())
      ->set06HideShipping($hide_shipping)
      ->set07URLs($form['#return_url'], $payment_gateway_plugin->getNotifyUrl()->toString())
      ->set08Lang($this->languageManager->getCurrentLanguage()->getName())
      ->set09Framed($frammed, 'parent')
      ->set99PluginInfo('DrupalCommerce', $platform_version, $plugin_version);

    $shipping_profile = $payment_gateway_plugin->getShippingProfile($order);
    if ($shipping_profile) {
      $shipping_phone = $shipping_profile->hasField($config['telephone_field']) ? $shipping_profile->get($config['telephone_field'])->value : 00000000000;
      $shipping_info = $shipping_profile->address->first();
      $shipping_full_name = $shipping_info->getGivenName() . ' ' . $shipping_info->getFamilyName();
      $shipping_country_code = $shipping_info->getCountryCode();
      $shipping_country = $this->countryRepository->get($shipping_country_code)->getThreeLetterCode();
      $paytabs_core->set05ShippingDetails(FALSE, $shipping_full_name, $customer->getEmail(), $shipping_phone, $shipping_info->getAddressLine1(), $shipping_info->getLocality(), $shipping_info->getAdministrativeArea() ? str_replace(" Governorate", "", $shipping_info->getAdministrativeArea()) : $shipping_info->getLocality(), $shipping_country, $shipping_info->getPostalCode() ? $shipping_info->getPostalCode() : '00000', $order->getIpAddress());
    }

    if ($config['reuse_payment_method']) {
      $paytabs_core->set11TokeniseInfo(TRUE, 2, PaytabsEnum::TOKEN_TYPE_REGISTERED);
    }
    $pp_params = $paytabs_core->pt_build();
    $response = $paytabs_api->create_pay_page($pp_params);

    if ($response->success) {
      $redirect_url = $response->redirect_url;
      $form['commerce_message']['#action'] = $redirect_url;
      $redirect_method = 'post';

      if ($frammed === TRUE) {
        $form['#attributes']['id'][] = 'paymentWidgets';
        $form['#attached']['drupalSettings']['commerce_paytabs']['redirect_url'] = $redirect_url;
        $form['#attached']['drupalSettings']['commerce_paytabs']['return_url'] = $form['#return_url'];
        $form['#attached']['library'][] = 'commerce_paytabs/hosted_page';

        // No need to call buildRedirectForm(), as we embed an iframe.
        return $form;
      }
      else {
        return $this->buildRedirectForm($form, $form_state, $redirect_url, $pp_params, $redirect_method);
      }

    }
    else {
      $this->messenger->addStatus($this->t('Something went wrong, please try again later'));
      $this->logger->error('failed to create payment page for order and response from paytabs is :' . $response->message);
    }
  }

}
