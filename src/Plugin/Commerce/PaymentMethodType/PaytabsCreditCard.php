<?php

namespace Drupal\commerce_paytabs\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the paytabs credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "paytabs_credit_card",
 *   label = @Translation("Credit card"),
 * )
 */
class PaytabsCreditCard extends CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['trans_ref'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Transaction Reference'))
      ->setDescription($this->t('The initial payment transaction reference.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
