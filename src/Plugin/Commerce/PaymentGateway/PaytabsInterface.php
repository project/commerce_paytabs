<?php

namespace Drupal\commerce_paytabs\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;

/**
 * Provides the interface for the PayTabs payment gateway.
 */
interface PaytabsInterface extends OffsitePaymentGatewayInterface, SupportsRefundsInterface, SupportsStoredPaymentMethodsInterface, SupportsAuthorizationsInterface {

}
