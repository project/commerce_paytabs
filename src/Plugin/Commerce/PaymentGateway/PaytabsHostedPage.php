<?php

namespace Drupal\commerce_paytabs\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_paytabs\PaytabsApi;
use Drupal\commerce_paytabs\PaytabsEnum;
use Drupal\commerce_paytabs\PaytabsFollowupHolder;
use Drupal\commerce_paytabs\PaytabsSDK;
use Drupal\commerce_paytabs\PaytabsTokenHolder;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Paytabs hosted payment page gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paytabs_hosted_page",
 *   label = @Translation("Paytabs hosted page"),
 *   display_label = @Translation("Paytabs"),
 *   forms = {
 *     "add-payment" = "Drupal\commerce_payment\PluginForm\ManualPaymentAddForm",
 *     "offsite-payment" = "Drupal\commerce_paytabs\PluginForm\HostedPage\PaymentPage",
 *   },
 *   payment_method_types = {"paytabs_credit_card"},
 *   credit_card_types = {
 *     "visa", "mastercard",
 *   },
 * )
 */
class PaytabsHostedPage extends OffsitePaymentGatewayBase implements PaytabsInterface {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->paytabsSdk = new PaytabsSDK();
    $instance->logger = $container->get('logger.factory')->get('commerce_paytabs');
    $instance->moduleExtensionList = $container->get('extension.list.module');
    $instance->languageManager = $container->get('language_manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->countryRepository = $container->get('address.country_repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'profile_id' => '',
      'server_key' => '',
      'region' => '',
      'pay_page_mode' => '',
      'iframe' => '',
      'telephone_field' => '',
      'complete_order_status' => '',
      'hide_shipping_address' => '',
      'reuse_payment_method' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['profile_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Merchant Profile id'),
      '#required' => TRUE,
      '#description' => $this->t('Your merchant profile id , you can find the profile id on your ayTabs Merchant’s Dashboard- profile.'),
      '#default_value' => $this->configuration['profile_id'],
    ];
    $form['server_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Server Key'),
      '#description' => $this->t('You can find the Server key on your PayTabs Merchant’s Dashboard - Developers - Key management.'),
      '#default_value' => $this->configuration['server_key'],
    ];
    $regions = [];
    foreach (PaytabsApi::BASE_URLS as $key => $value) {
      $regions[$key] = $value['title'];
    }
    $form['region'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Merchant region'),
      '#description' => $this->t('The region you registered in with PayTabs.'),
      '#options' => $regions,
      '#default_value' => $this->configuration['region'],
    ];
    $form['pay_page_mode'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Pay Page Mode'),
      '#description' => $this->t('The mode you need to process payment with.'),
      '#options' => [
        'sale' => $this->t('Sale'),
        'auth' => $this->t('Auth'),
      ],
      '#default_value' => $this->configuration['pay_page_mode'],
    ];

    $form['iframe'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Integration Mode'),
      '#description' => $this->t('The mode you need to integrate with.'),
      '#options' => [
        'false' => $this->t('Redirect outside the site'),
        'true' => $this->t('Iframe inside the site'),
      ],
      '#default_value' => $this->configuration['iframe'],
    ];

    $field_options = [];
    $fields = $this->entityFieldManager->getFieldDefinitions('profile', 'customer');
    foreach ($fields as $key => $value) {
      if ($value instanceof FieldConfig) {
        $field_options[$value->getName()] = $value->getLabel();
      }
    }
    $profile_route = Url::fromRoute("entity.profile.field_ui_fields", ['profile_type' => 'customer'])->toString();
    $form['telephone_field'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Customer profile telephone field'),
      '#options' => $field_options,
      '#description' => $this->t('Paytabs require the customer profile telephone number, you can add new field from <a href="@profile-managment">here.</a>', ['@profile-managment' => $profile_route]),
      '#default_value' => $this->configuration['telephone_field'],
    ];

    $form['hide_shipping_address'] = [
      '#type' => 'select',
      '#required' => FALSE,
      '#title' => $this->t('Hide shipping address'),
      '#description' => $this->t('Hide shipping address.'),
      '#options' => [
        'false' => $this->t('Show shipping address'),
        'true' => $this->t('Hide shipping address'),
      ],
      '#default_value' => $this->configuration['hide_shipping_address'],
    ];

    $form['complete_order_status'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Order Status'),
      '#description' => $this->t('Order status after payment is done'),
      '#options' => [
        'completed' => $this->t("completed '(this status is used when no action is needed )'"),
        'fulfillment' => $this->t("fulfillment '(if you select this option you should to use order fulfillment workflow)'"),
        'validation' => $this->t("validation '(if you select this option you should to use order default with validation workflow)'"),
      ],
      '#default_value' => $this->configuration['complete_order_status'],
    ];

    $form['reuse_payment_method'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow reusing payment method'),
      '#description' => $this->t('Allow saving card details to be used later without the need to re enter the card details'),
      '#default_value' => $this->configuration['reuse_payment_method'],
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['profile_id'] = $values['profile_id'];
    $this->configuration['server_key'] = $values['server_key'];
    $this->configuration['region'] = $values['region'];
    $this->configuration['pay_page_mode'] = $values['pay_page_mode'];
    $this->configuration['iframe'] = $values['iframe'];
    $this->configuration['telephone_field'] = $values['telephone_field'];
    $this->configuration['hide_shipping_address'] = $values['hide_shipping_address'];
    $this->configuration['complete_order_status'] = $values['complete_order_status'];
    $this->configuration['reuse_payment_method'] = $values['reuse_payment_method'];
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $paytabs_core = new PaytabsTokenHolder();
    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);

    $order = $payment->getOrder();
    $payment_amount = $payment->getAmount();
    $customer = $order->getCustomer();

    $transaction_title = $this->t('Order Number: @order_number', ['@order_number' => $order->id()]);
    $platform_version = $this->moduleExtensionList->getExtensionInfo('commerce')['version'] ?? 0;
    $plugin_version = $this->moduleExtensionList->getExtensionInfo('commerce_paytabs')['version'] ?? 0;

    $billing_profile = $order->getBillingProfile();
    if ($billing_profile->hasField($this->configuration['telephone_field'])) {
      $phone = $billing_profile->get($this->configuration['telephone_field'])->value;
    }
    else {
      $phone = 00000000000;
    }
    $billing_info = $billing_profile->address->first();
    $billing_country_code = $billing_info->getCountryCode();
    $billing_country = $this->countryRepository->get($billing_country_code)->getThreeLetterCode();
    $billing_full_name = $billing_info->getGivenName() . ' ' . $billing_info->getFamilyName();

    $paytabs_core
      ->set02Transaction($this->configuration['pay_page_mode'], 'recurring')
      ->set03Cart($order->id(), $payment_amount->getCurrencyCode(), floatval($payment_amount->getNumber()), $transaction_title)
      ->set04CustomerDetails($billing_full_name, $customer->getEmail(), $phone, $billing_info->getAddressLine1(), $billing_info->getLocality(), $billing_info->getAdministrativeArea() ? str_replace(" Governorate", "", $billing_info->getAdministrativeArea()) : $billing_info->getLocality(), $billing_country, $billing_info->getPostalCode() ? $billing_info->getPostalCode() : '00000', $order->getIpAddress())
      ->set05ShippingDetails(TRUE)
      ->set06HideShipping($this->configuration['hide_shipping_address'])
      ->set20Token($payment_method->trans_ref->value, $payment_method->remote_id->value)
      ->set99PluginInfo('DrupalCommerce', $platform_version, $plugin_version);

    $shipping_profile = $this->getShippingProfile($order);
    if ($shipping_profile) {
      $shipping_phone = $shipping_profile->hasField($this->configuration['telephone_field']) ? $shipping_profile->get($this->configuration['telephone_field'])->value : 00000000000;
      $shipping_info = $shipping_profile->address->first();
      $shipping_full_name = $shipping_info->getGivenName() . ' ' . $shipping_info->getFamilyName();
      $shipping_country_code = $shipping_info->getCountryCode();
      $shipping_country = $this->countryRepository->get($shipping_country_code)->getThreeLetterCode();
      $paytabs_core->set05ShippingDetails(FALSE, $shipping_full_name, $customer->getEmail(), $shipping_phone, $shipping_info->getAddressLine1(), $shipping_info->getLocality(), $shipping_info->getAdministrativeArea() ? str_replace(" Governorate", "", $shipping_info->getAdministrativeArea()) : $shipping_info->getLocality(), $shipping_country, $shipping_info->getPostalCode() ? $shipping_info->getPostalCode() : '00000', $order->getIpAddress());
    }

    $pp_params = $paytabs_core->pt_build();
    $transaction = $paytabs_api->create_pay_page($pp_params);

    $trans_ref = $transaction->tran_ref;
    $respStatus = $transaction->payment_result->response_status;
    $transaction_type = $transaction->tran_type;
    if (!$transaction->success) {
      throw new PaymentGatewayException($transaction->message);
    }
    $this->logger->info('return Payment information. Transaction reference: ' . $trans_ref);
    if ($respStatus === 'A') {
      $message = 'Your payment was successful to payTabs with Transaction reference ';
      if ($transaction_type === 'Sale') {
        $payment_status = 'completed';
      }
      elseif ($transaction_type === 'Auth') {
        $payment_status = 'authorization';
      }

      $this->messenger()->addStatus($message . ' : ' . $trans_ref);
    }
    elseif ($respStatus === 'C') {
      $message = 'Your payment was Cancelled with Transaction reference ';
      $payment_status = 'cancelled';
      $this->messenger()->addError($message . ' : ' . $trans_ref);
    }
    else {
      $message = 'Your payment was ' . $all_response['respMessage'] . 'with Transaction reference ';
      $payment_status = $all_response['respMessage'];
      $this->messenger()->addWarning($message . ' : ' . $trans_ref);
    }

    // Check if order don't have payments to insert it.
    $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties([
      'order_id' => $order->id(),
      'remote_id' => $trans_ref,
      'remote_state' => $respStatus,
    ]);

    if (empty($payments)) {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => $payment_status,
        'amount' => $order->getTotalPrice(),
        'payment_gateway' => $this->entityId,
        'order_id' => $order->id(),
        'remote_id' => $trans_ref,
        'remote_state' => $respStatus,
        'authorized' => $this->time->getRequestTime(),
      ]);
      $this->logger->info('Saving Payment information. Transaction reference: ' . $trans_ref);

      $payment->save();
      $this->logger->info('Payment information saved successfully. Transaction reference: ' . $trans_ref);
    }

    $order->set('state', $this->configuration['complete_order_status']);
    $order->save();
    $this->logger->info('order status updated successfully.');
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, Request $request) {
    $payment_method->save();
    return $payment_method;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    $all_response = $request->request->all();

    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);

    $is_valid = $paytabs_api->is_valid_redirect($all_response);

    if (!$is_valid) {
      $this->messenger()->addError($this->t('not valid result from PayTabs'));
      $this->logger->info('not valid result from PayTabs.');
      throw new PaymentGatewayException("Failed");
    }
    else {
      $trans_ref = $request->request->get('tranRef');
      $transaction = $paytabs_api->verify_payment($trans_ref);
      $transaction_type = $transaction->tran_type;
      $respStatus = $transaction->payment_result->response_status;
      if (!$transaction->success) {
        throw new PaymentGatewayException($transaction->message);
      }
      $this->logger->info('return Payment information. Transaction reference: ' . $trans_ref);
      if ($respStatus === 'A') {
        $message = 'Your payment was successful to payTabs with Transaction reference ';
        if ($transaction_type === 'Sale') {
          $payment_status = 'completed';
        }
        elseif ($transaction_type === 'Auth') {
          $payment_status = 'authorization';
        }

        $this->messenger()->addStatus($message . ' : ' . $trans_ref);
      }
      elseif ($respStatus === 'C') {
        $message = 'Your payment was Cancelled with Transaction reference ';
        $payment_status = 'cancelled';
        $this->messenger()->addError($message . ' : ' . $trans_ref);
      }
      else {
        $message = 'Your payment was ' . $all_response['respMessage'] . 'with Transaction reference ';
        $payment_status = $all_response['respMessage'];
        $this->messenger()->addWarning($message . ' : ' . $trans_ref);
      }

      // Check if order don't have payments to insert it.
      $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties([
        'order_id' => $order->id(),
        'remote_id' => $trans_ref,
        'remote_state' => $respStatus,
      ]);

      if (empty($payments)) {
        $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
        $payment = $payment_storage->create([
          'state' => $payment_status,
          'amount' => $order->getTotalPrice(),
          'payment_gateway' => $this->entityId,
          'order_id' => $order->id(),
          'remote_id' => $trans_ref,
          'remote_state' => $respStatus,
          'authorized' => $this->time->getRequestTime(),
        ]);
        $this->logger->info('Saving Payment information. Transaction reference: ' . $trans_ref);

        if ($this->configuration['reuse_payment_method']) {
          $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
          $payment_method = $payment_method_storage->createForCustomer(
            'paytabs_credit_card',
            $this->parentEntity->id(),
            $order->getCustomerId(),
            $order->getBillingProfile()
          );
          $payment_method->card_type = strtolower($transaction->payment_info->card_scheme);
          $payment_method->card_number = substr($transaction->payment_info->payment_description, -4);
          $payment_method->card_exp_month = $transaction->payment_info->expiryMonth;
          $payment_method->card_exp_year = $transaction->payment_info->expiryYear;
          $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);
          $payment_method->setExpiresTime($expires);
          $payment_method->setRemoteId($transaction->token);
          $payment_method->trans_ref = $trans_ref;
          $payment_method = $this->createPaymentMethod($payment_method, $request);
          $order->set('payment_method', $payment_method->id());
          $payment->set('payment_method', $payment_method->id());
        }
        $payment->save();
        $this->logger->info('Payment information saved successfully. Transaction reference: ' . $trans_ref);
      }
      $order->set('state', $this->configuration['complete_order_status']);
      $order->save();
      $this->logger->info('order status updated successfully.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $status = $request->get('status');
    $this->messenger()->addError('Payment @status on @gateway but may resume the checkout process here when you are ready.', [
      '@status' => $status,
      '@gateway' => $this->getDisplayLabel(),
    ], 'error');
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $decimal_amount = $amount->getNumber();
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $remote_id = $payment->getRemoteId();
    $cart_id = $payment->getOrder()->id();

    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);
    $refund = new PaytabsFollowupHolder();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    try {
      $refund->set02Transaction(PaytabsEnum::TRAN_TYPE_REFUND, PaytabsEnum::TRAN_CLASS_ECOM)
        ->set03Cart($cart_id, $currency_code, $decimal_amount, 'refunded from drupal')
        ->set30TransactionInfo($remote_id);

      $refund_params = $refund->pt_build();
      $result = $paytabs_api->request_followup($refund_params);

      $success = $result->success;
      $message = $result->message;
      $pending_success = $result->pending_success;

      if ($success) {
        // Determine whether payment has been fully or partially refunded.
        $old_refunded_amount = $payment->getRefundedAmount();
        $new_refunded_amount = $old_refunded_amount->add($amount);
        if ($new_refunded_amount->lessThan($payment->getAmount())) {
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }
        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();
      }
      elseif ($pending_success) {
        $this->messenger()->addError($this->t('Not valid result from PayTabs<br>@message', ['@message' => $message]));
        $this->logger->info('Not valid result from PayTabs<br>' . $message);
      }
    }
    catch (\Exception $e) {
      $this->logger->log('error', 'failed to proceed to refund transaction:' . $remote_id . "<br>" . $message);
      throw new PaymentGatewayException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $decimal_amount = $amount->getNumber();
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $remote_id = $payment->getRemoteId();
    $cart_id = $payment->getOrder()->id();

    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);
    $capture = new PaytabsFollowupHolder();

    // To prevent from capture more than the order value.
    $this->assertAmount($payment, $amount, 'Capture');

    // Perform the refund request here, throw an exception if it fails.
    try {
      $capture->set02Transaction(PaytabsEnum::TRAN_TYPE_CAPTURE, PaytabsEnum::TRAN_CLASS_ECOM)
        ->set03Cart($cart_id, $currency_code, $decimal_amount, 'Capture from drupal')
        ->set30TransactionInfo($remote_id);

      $capture_params = $capture->pt_build();
      $result = $paytabs_api->request_followup($capture_params);

      $success = $result->success;
      $message = $result->message;
      $pending_success = $result->pending_success;

      if ($success) {
        // Partial capture.
        if ($amount < $payment->getAmount()) {
          // Update the authorized record with the remaining value.
          $new_amount = $payment->getBalance()->subtract($amount);
          $payment->setAmount($new_amount);
          $payment->save();

          // Create new payment record for capture transacion.
          $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
          $payment = $payment_storage->create([
            'state' => 'completed',
            'amount' => $amount,
            'payment_gateway' => $this->entityId,
            'order_id' => $result->cart_id,
            'remote_id' => $result->tran_ref,
            'remote_state' => $result->payment_result->response_status,
            'authorized' => $this->time->getRequestTime(),
          ]);
          $this->logger->info('Saving Payment information. Transaction reference: ' . $result->tran_ref);
          $payment->save();
          $this->logger->info('Payment information saved successfully. Transaction reference: ' . $result->tran_ref);

        }
        // Full capture.
        else {
          $payment->setState('completed');
          $payment->setRemoteId($result->tran_ref);
          $payment->setAmount($amount);
          $payment->save();
        }

      }
      elseif ($pending_success) {
        $this->messenger()->addError($this->t('Not valid result from PayTabs<br>@message', ['@message' => $message]));
        $this->logger->info('Not valid result from PayTabs<br>' . $message);
      }
    }
    catch (\Exception $e) {
      $this->logger->log('error', 'failed to proceed to capture transaction:' . $remote_id . "<br>" . $message);
      throw new PaymentGatewayException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $payment->getAmount();
    $decimal_amount = $amount->getNumber();
    $currency_code = $payment->getAmount()->getCurrencyCode();
    $remote_id = $payment->getRemoteId();
    $cart_id = $payment->getOrder()->id();

    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);
    $void = new PaytabsFollowupHolder();

    // Perform the refund request here, throw an exception if it fails.
    try {
      $void->set02Transaction(PaytabsEnum::TRAN_TYPE_VOID, PaytabsEnum::TRAN_CLASS_ECOM)
        ->set03Cart($cart_id, $currency_code, $decimal_amount, 'void from drupal')
        ->set30TransactionInfo($remote_id);

      $capture_params = $void->pt_build();
      $result = $paytabs_api->request_followup($capture_params);

      $success = $result->success;
      $message = $result->message;
      $pending_success = $result->pending_success;

      if ($success) {
        $payment->setState('authorization_voided');
        $payment->setRemoteId($result->tran_ref);
        $payment->save();
      }
      elseif ($pending_success) {
        $this->messenger()->addError($this->t('Not valid result from PayTabs<br>@message', ['@message' => $message]));
        $this->logger->info('Not valid result from PayTabs<br>' . $message);
      }
    }
    catch (\Exception $e) {
      $this->logger->log('error', 'failed to proceed to void transaction:' . $remote_id . "<br>" . $message);
      throw new PaymentGatewayException($e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $paytabs_api = PaytabsApi::getInstance($this->configuration['region'], $this->configuration['profile_id'], $this->configuration['server_key']);
    $paytabs_api->token_delete($payment_method->remote_id->value);
    $payment_method->delete();
  }

  /**
   * Get order shipping profile.
   */
  public function getShippingProfile(OrderInterface $order) {
    if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
        return $shipment->getShippingProfile();
      }
    }

    return FALSE;
  }

  /**
   * Assert ampunt.
   */
  public function assertAmount(PaymentInterface $payment, Price $amount, $type) {
    $balance = $payment->getBalance();
    if ($amount->greaterThan($balance)) {
      throw new InvalidRequestException(sprintf("Can't " . $type . " more than %s.", $balance->__toString()));
    }
  }

}
