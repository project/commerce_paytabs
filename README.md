# Commerce Paytabs

## CONTENTS OF THIS FILE

- Introduction
- Requirements
- Installation
- Configuration
- How it works
- Maintainers

## INTRODUCTION

**Commerce PayTabs** is [Drupal Commerce](https://drupal.org/project/commerce)
module that integrates the [paytabs.com](https://www.paytabs.com/) payement
gateway into your Drupal Commerce shop.

## REQUIREMENTS

This module requires the following:

- Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
  - Telephone
- paytabs.com account (test or live) (https://www.paytabs.com/)

## INSTALLATION

Open your terminal and download with composer.
composer require drupal/commerce_paytabs

OR

1. Download the module to your DRUPAL_ROOT/modules directory,
   or where ever you install contrib modules on your site.
2. Go to Admin > Extend and enable the module.

## CONFIGURATION

- Create a new telephone field from /admin/config/people/profile-types/manage/customer/fields
  to allow the user to enter thier phone number during checkout process.
- Create a new PayTabs Hosted page payment gateway.
  Administration > Commerce > Configuration > Payment gateways > Add payment gateway
  Paytabs hosted page specific settings available:
  - Merchant Profile id
  - Server Key
  - Merchant region
  - Pay Page Mode
  - Integration Mode
  - Customer profile telephone field, select the telephone field you created in the previous step
  - Hide shipping address
  - Order Status
  - Allow reusing payment method

## HOW IT WORKS

- HostedPage
  HostedPage payment has two options: Iframe embeded on the checkout page, or redirect to the payment page.

- 3DS support:
  Handled by the HyperPay HostedPage.

- Checkout workflow:
  It follows the Drupal Commerce Credit Card workflow.
  The customer should enter his/her credit card data
  or select one of the credit cards saved with Paytabs
  from a previous order.

- Payment Terminal
  The store owner can Void, Capture and Refund the PayTabs payments from the order page.

## MAINTAINERS

- Anas Mawlawi (Anas_maw) - <https://www.drupal.org/u/anas_maw>
- Yasser Samman (yasser-samman) - https://www.drupal.org/u/yasser-samman

This 2.0.x branch has been developed by:

- Coders Enterprise Web & Mobile Solutions: https://www.codersme.com/
