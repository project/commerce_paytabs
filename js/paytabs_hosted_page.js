(function ($, Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.offsiteForm = {
    attach: function (context) {
      var ptLink = drupalSettings.commerce_paytabs.redirect_url;
      var return_link = drupalSettings.commerce_paytabs.return_url;
      var iframe = document.createElement("iframe");
      iframe.width = "100%";
      iframe.height = "900px";
      iframe.id = "PaytabsIframe";
      iframe.setAttribute("src", ptLink);
      var $form = $(context).find(".checkout-pane");
      if ($form.length > 0) {
        $(once("#paymentWidgets", $form)).append(iframe);
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
